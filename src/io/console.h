#pragma once

#include "input_parser.h"
#include "output_printer.h"

namespace io
{

class console
{
public:
  console(std::istream &in, std::ostream &out);

  void print_prompt();
  void print_message(std::string const &message);
  void print_list(std::vector<std::string> const& list);

  [[nodiscard]] parse_result query_input();

private:
  input_parser parser_;
  output_printer printer_;
};

}