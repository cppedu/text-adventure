#include "output_printer.h"

#include <vector>

namespace io
{

output_printer::output_printer(std::ostream &stream)
  : stream_(stream)
{
}

void output_printer::print(std::string const &text)
{
  stream_ << text;
}

void output_printer::print_message(std::string const &message)
{
  auto words = split(message);

  unsigned int line_size = print_begin_of_line();
  for (auto const& word : words) {
    if (!fits_in_line(line_size, word)) {
      stream_ << "\n";
      line_size = print_begin_of_line();
    }

    stream_ << " " << word;
    line_size += 1 + word.size();
  }

  stream_ << "\n";
}

unsigned int output_printer::print_begin_of_line() const
{
  stream_ << line_begin;
  return line_begin.size();
}

bool output_printer::fits_in_line(unsigned int current_line_size, std::string const &word) const
{
  return current_line_size + 1 + word.size() < line_size_limit;
}

std::vector<std::string> output_printer::split(std::string const &str, char delimiter) const
{
  std::vector<std::string> strings;

  for (size_t first=0, last=0; last < str.length(); first=last+1) {
    last = str.find(delimiter, first);
    strings.push_back(str.substr(first, last-first));
  }

  return strings;
}

}