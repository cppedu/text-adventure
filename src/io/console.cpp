#include <sstream>
#include "console.h"

namespace io
{

console::console(std::istream &in, std::ostream &out)
  : parser_{in},
    printer_{out}
{
}

parse_result console::query_input()
{
  for (;;) {
    print_prompt();
    if (auto [success, result] = parser_.parse(); success)
      return result;

    print_message("Please type a verb and (optionally) a noun to explain what you want to do.");
  }
}

void console::print_prompt()
{
  printer_.print("> ");
}

void console::print_message(std::string const &message)
{
  printer_.print_message(message);
}

void console::print_list(std::vector<std::string> const &list)
{
  for (auto const& item : list)
    printer_.print("   * " + item + "\n");
  printer_.print("\n");
}

}