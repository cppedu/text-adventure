#pragma once

#include <string>
#include <iostream>
#include <vector>

namespace io
{

using namespace std::string_view_literals;

class output_printer
{
  static constexpr auto line_size_limit = 80u;
  static constexpr auto line_begin = "  "sv;
public:
  explicit output_printer(std::ostream &stream);

  void print(std::string const& text);
  void print_message(std::string const &message);

private:
  std::ostream &stream_;

  [[nodiscard]] std::vector<std::string> split(std::string const& str, char delimiter = ' ') const;

  [[nodiscard]] bool fits_in_line(unsigned int current_line_size, std::string const &word) const;

  [[nodiscard]] unsigned int print_begin_of_line() const;
};

}