#include "input_parser.h"

#include <sstream>

namespace io
{

input_parser::input_parser(std::istream &stream)
  : stream_{stream}
{
}

std::pair<bool, parse_result> input_parser::parse()
{
  std::string line;
  getline(stream_, line);

  std::stringstream line_stream{line};

  parse_result result;
  line_stream >> result.verb;

  std::string optional_noun;
  line_stream >> optional_noun;
  if (!optional_noun.empty())
    result.noun = optional_noun;

  return {!result.verb.empty(), result};
}

}