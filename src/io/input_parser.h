#pragma once

#include <string>
#include <iostream>
#include <optional>

namespace io
{

struct parse_result
{
  std::string verb;
  std::optional<std::string> noun;
};

class input_parser
{
public:
  explicit input_parser(std::istream &stream);

  [[nodiscard]] std::pair<bool, parse_result> parse();

private:
  std::istream &stream_;
};

}