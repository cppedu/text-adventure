#include <fstream>
#include <iostream>
#include "content_repository.h"
#include "../world/locked_connection.h"

namespace storage
{

std::vector<std::unique_ptr<world::room>> &content_repository::rooms()
{
  return rooms_;
}

std::vector<std::unique_ptr<items::item>> &content_repository::items()
{
  return items_;
}

bool content_repository::load(std::string const &filename)
{
  std::ifstream file{filename};

  if (!file)
    return false;

  while (!file.eof()) {
    std::string type;
    file >> type;

    if (type.empty())
      continue;

    auto section = section_type_from_string(type);
    if (!section)
      return false;

    switch (section.value()) {
    case section_type::item:
      if (!load_items(file))
        return false;
      break;

    case section_type::room:
      if (!load_rooms(file))
        return false;
      break;

    case section_type::connection:
      if (!load_connections(file))
        return false;
      break;

    case section_type::item_placement:
      if (!load_item_placement(file))
        return false;
      break;

    case section_type::npc:
      return true; // TODO
      break;
    }
  }

  return true;
}

bool content_repository::load_items(std::ifstream &file)
{
  item_factory factory;

  std::size_t entry_count = 0;
  file >> entry_count;

  for (auto i = 0u; i < entry_count; ++i) {
    int id = 0;
    file >> id;
    if (id != i)
      return false;
    skip_until_nextline(file);

    std::string type;
    std::getline(file, type);

    auto item_type = item_type_from_string(type);
    if (!item_type)
      return false;

    std::string name;
    std::getline(file, name);

    std::string description;
    std::getline(file, description);

    items_.push_back(factory.create(item_type.value(), name, description));
  }

  return true;
}

bool content_repository::load_rooms(std::ifstream &file)
{
  std::size_t entry_count = 0;
  file >> entry_count;

  for (auto i = 0u; i < entry_count; ++i) {
    int id = 0;
    file >> id;
    if (id != i)
      return false;
    skip_until_nextline(file);

    std::string name;
    std::getline(file, name);

    std::string enter_message;
    std::getline(file, enter_message);

    std::string inspect_message;
    std::getline(file, inspect_message);

    rooms_.push_back(std::make_unique<world::room>(name, enter_message, inspect_message));
  }

  return true;
}

bool content_repository::load_connections(std::ifstream &file)
{
  connection_factory factory;

  std::size_t entry_count = 0;
  file >> entry_count;

  for (auto i = 0u; i < entry_count; ++i) {
    int id = 0;
    file >> id;
    if (id != i)
      return false;
    skip_until_nextline(file);

    std::string type;
    std::getline(file, type);

    auto connection_type = connection_type_from_string(type);
    if (!connection_type)
      return false;

    auto [success_a, room_a, traversal_direction_a_to_b] = load_connection_endpoint(file);
    if (!success_a)
      return false;

    auto [success_b, room_b, traversal_direction_b_to_a] = load_connection_endpoint(file);
    if (!success_b)
      return false;

    switch (connection_type.value()) {
    case connection_type::open:
      room_a->add_connection(traversal_direction_a_to_b, std::make_unique<world::connection>(*room_b));
      room_b->add_connection(traversal_direction_b_to_a, std::make_unique<world::connection>(*room_a));
      break;

    case connection_type::locked_by_item: {
      int item_id = 0;
      file >> item_id;

      if (item_id < 0 && item_id >= items_.size())
        return false;
      auto *item = items_[item_id].get();

      auto connection_a_to_b = std::make_unique<world::locked_connection>(*room_b);
      connection_a_to_b->set_key(*item);
      room_a->add_connection(traversal_direction_a_to_b, std::move(connection_a_to_b));

      auto connection_b_to_a = std::make_unique<world::locked_connection>(*room_a);
      connection_b_to_a->set_key(*item);
      room_b->add_connection(traversal_direction_b_to_a, std::move(connection_b_to_a));
      }
      break;
    }

    skip_until_nextline(file);
  }

  return true;
}

std::tuple<bool, world::room *, world::direction> content_repository::load_connection_endpoint(std::ifstream &file)
{
  int endpoint_id = 0;
  std::string direction_name;

  file >> endpoint_id >> direction_name;
  auto direction = direction_from_string(direction_name);
  if (!direction || endpoint_id < 0 || endpoint_id > rooms_.size())
    return {false, nullptr, {}};

  return {true, rooms_[endpoint_id].get(), direction.value()};
}

bool content_repository::load_item_placement(std::ifstream &file)
{
  std::size_t entry_count = 0;
  file >> entry_count;

  for (auto i = 0u; i < entry_count; ++i) {
    int id = 0;
    file >> id;
    if (id != i)
      return false;

    int item_id = 0, room_id = 0;
    file >> item_id
         >> room_id;

    if (item_id < 0 && item_id >= items_.size())
      return false;
    auto *item = items_[item_id].get();

    if (room_id < 0 && room_id >= rooms_.size())
      return false;
    auto *room = rooms_[room_id].get();

    room->add_item(*item);

    skip_until_nextline(file);
  }

  return true;
}

void content_repository::skip_until_nextline(std::ifstream &file)
{
  std::string type;
  std::getline(file, type);
}

std::optional<section_type> content_repository::section_type_from_string(std::string const &type) const
{
  if (type == "ITEM")
    return section_type::item;
  else if (type == "ROOM")
    return section_type::room;
  else if (type == "CONNECTION")
    return section_type::connection;
  else if (type == "ITEM_PLACEMENT")
    return section_type::item_placement;
  else if (type == "NPC")
    return section_type::npc;
  else
    return {};
}

std::optional<item_type> content_repository::item_type_from_string(std::string const &type) const
{
  if (type == "KEY")
    return item_type::key;
  else if (type == "ROPE")
    return item_type::rope;
  else
    return {};
}

std::optional<connection_type> content_repository::connection_type_from_string(std::string const &type) const
{
  if (type == "OPEN")
    return connection_type::open;
  else if (type == "LOCKED_BY_ITEM")
    return connection_type::locked_by_item;
  else
    return {};
}

std::optional<world::direction> content_repository::direction_from_string(std::string const &direction) const
{
  if (direction == "NORTH")
    return world::direction::north;
  else if (direction == "EAST")
    return world::direction::east;
  else if (direction == "SOUTH")
    return world::direction::south;
  else if (direction == "WEST")
    return world::direction::west;
  else
    return {};
}

}