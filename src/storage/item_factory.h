#pragma once

#include <memory>
#include "../items/item.h"

namespace storage
{

enum class item_type
{
  key,
  rope,
};

class item_factory
{
public:
  [[nodiscard]] std::unique_ptr<items::item>
  create(item_type type, std::string const &name, std::string const &description) const;
};

}