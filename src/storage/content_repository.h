#pragma once

#include <vector>
#include <memory>
#include "../world/room.h"
#include "item_factory.h"
#include "connection_factory.h"

namespace storage
{

enum class section_type
{
  item,
  room,
  connection,
  item_placement,
  npc,
};

class content_repository
{
public:
  std::vector<std::unique_ptr<world::room>> &rooms();
  std::vector<std::unique_ptr<items::item>> &items();

  bool load(std::string const &filename);

private:
  std::vector<std::unique_ptr<world::room>> rooms_;
  std::vector<std::unique_ptr<items::item>> items_;

  bool load_items(std::ifstream &file);
  bool load_rooms(std::ifstream &file);
  bool load_connections(std::ifstream &file);
  [[nodiscard]] std::tuple<bool, world::room *, world::direction>
  load_connection_endpoint(std::ifstream &file);
  bool load_item_placement(std::ifstream &file);

  void skip_until_nextline(std::ifstream &file);

  std::optional<section_type> section_type_from_string(std::string const &type) const;
  std::optional<item_type> item_type_from_string(std::string const &type) const;
  std::optional<connection_type> connection_type_from_string(std::string const &type) const;
  std::optional<world::direction> direction_from_string(std::string const &direction) const;
};

}