#include "item_factory.h"
#include "../items/key.h"
#include "../items/rope.h"

namespace storage
{

std::unique_ptr<items::item>
item_factory::create(item_type type, std::string const &name, std::string const &description) const
{
  switch (type) {
  case item_type::key:
    return std::make_unique<items::key>(name, description);

  case item_type::rope:
    return std::make_unique<items::rope>(name, description);

  default:
    return nullptr;
  }
}

}